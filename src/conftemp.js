import {gettemp} from './temp'
import {criaPedidos} from './pedidos'
import { addCardapioMolho, addCardapioMolhoEspetinho, addCardapioRecheios } from './molhosrecheios'

if(gettemp().length < 1){
    location.assign('./confirma.html')
}

if(gettemp()[0].nrDoPedido == 26){
    addCardapioMolho()
}

if(gettemp()[0].nrDoPedido == 34){
    addCardapioMolhoEspetinho()
}

if(gettemp()[0].nrDoPedido == 91){
    addCardapioRecheios()
}

//mostra o nome do pedido na pagina
const mostraNomeDoPedido = document.querySelector('#nomePedido')
mostraNomeDoPedido.textContent = gettemp()[0].produto
//pega o q foi digitado em obs
const pegaObs = document.querySelector('#insereObs')

//botao que esta na page
const botaoConfirmaInsere = document.querySelector('#confirmaInsere')

botaoConfirmaInsere.addEventListener('click', (e) => {
    e.preventDefault()
    if(gettemp()[0].nrDoPedido == 26){
        //pega o q foi selecionado no radiobox
        const inforadio = document.getElementById('chkMolhomolhodacasa')
        const inforadio2 = document.getElementById('chkMolhogeleiadepimenta')
        const inforadio3 = document.getElementById('chkMolhomostardaemel')
        const inforadio4 = document.getElementById('chkMolhobarbecue')

        const nr26 = gettemp()[0].nrDoPedido
        
        if(inforadio.checked == true && pegaObs.value != undefined && nr26 == 26){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Molho: '+ inforadio.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
        }else if(inforadio2.checked == true && pegaObs.value != undefined){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Molho: '+ inforadio2.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
        }else if(inforadio3.checked == true && pegaObs.value != undefined){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Molho: '+ inforadio3.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
        }else if(inforadio4.checked == true && pegaObs.value != undefined){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Molho: '+ inforadio4.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
        }
    }

    if(gettemp()[0].nrDoPedido == 34){
        const inforadio = document.getElementById('chkMolhorose')
        const inforadio2 = document.getElementById('chkMolhogeleiadepimenta')

        const nr26 = gettemp()[0].nrDoPedido

        if(inforadio.checked == true && pegaObs.value != undefined && nr26 == 34){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Molho: '+ inforadio.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
        }else if(inforadio2.checked == true && pegaObs.value != undefined){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Molho: '+ inforadio2.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
        }
    }

    if(gettemp()[0].nrDoPedido == 91){
        const inforadio = document.getElementById('chkMolhoskin')
        const inforadio2 = document.getElementById('chkMolhokani')
        const inforadio3 = document.getElementById('chkMolhopeixecrocante')

        const nr91 = gettemp()[0].nrDoPedido

        if(inforadio.checked == true && pegaObs.value != undefined && nr91 == 91){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Recheio: '+ inforadio.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 4, 'embalagem', '', 302)
        }else if(inforadio2.checked == true && pegaObs.value != undefined){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Recheio: '+ inforadio2.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 4, 'embalagem', '', 302)
        }else if(inforadio3.checked == true && pegaObs.value != undefined){
            criaPedidos(gettemp()[0].qtd, gettemp()[0].produto+ ' Recheio: '+ inforadio3.value + ' Obs: ' + pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
            criaPedidos(gettemp()[0].qtd, 'Embalagem', 4, 'embalagem', '', 302)
        }
    }

    if(gettemp()[0].nrDoPedido == 90){
        criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
        criaPedidos(gettemp()[0].qtd, 'Embalagem', 4, 'embalagem', '', 302)
    }

    if(gettemp()[0].nrDoPedido == 801 || gettemp()[0].nrDoPedido == 802 || gettemp()[0].nrDoPedido == 803 || gettemp()[0].nrDoPedido == 804 || gettemp()[0].nrDoPedido == 810 || gettemp()[0].nrDoPedido == 811 || gettemp()[0].nrDoPedido == 812){
        criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
        criaPedidos(gettemp()[0].qtd, 'Embalagem', 4, 'embalagem', '', 302)
        //location.assign('./confirma.html')
    }
    
    if(pegaObs.value !== undefined && gettemp()[0].nrDoPedido != 26 &&  gettemp()[0].nrDoPedido != 34 && gettemp()[0].nrDoPedido != 91 && gettemp()[0].nrDoPedido != 90 && gettemp()[0].nrDoPedido != 801 && gettemp()[0].nrDoPedido != 802 && gettemp()[0].nrDoPedido != 803 && gettemp()[0].nrDoPedido != 804 && gettemp()[0].nrDoPedido != 810 && gettemp()[0].nrDoPedido != 811 && gettemp()[0].nrDoPedido != 812){
        criaPedidos(gettemp()[0].qtd, gettemp()[0].produto + ' Obs: '+ pegaObs.value, gettemp()[0].preco, gettemp()[0].tipo, '', gettemp()[0].nrDoPedido)
        criaPedidos(gettemp()[0].qtd, 'Embalagem', 2, 'embalagem', '', 301)
    }

    if(gettemp()[0].nrDoPedido == 801 || gettemp()[0].nrDoPedido == 802 || gettemp()[0].nrDoPedido == 803 || gettemp()[0].nrDoPedido == 804 || gettemp()[0].nrDoPedido == 810 || gettemp()[0].nrDoPedido == 811 || gettemp()[0].nrDoPedido == 812){
        location.assign('./promocoes.html')
    }else{
        location.assign('./confadd.html')
    }
    
})