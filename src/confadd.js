import {getPedidos, criaPedidos} from './pedidos'
import {addCardapioAdicionais, addCardapioDinamics} from './molhosrecheios'
import {gettemp} from './temp'
import {gettemp2, apagatemp2} from './temp2'

addCardapioAdicionais()

if(gettemp()[0] == undefined){
    location.assign('./confirma.html')
}

if(gettemp2().length > 0){
    addCardapioDinamics()
}

//mostra o nome do pedido na pagina
const mostraNomeDoPedido = document.querySelector('#nomePedido')
mostraNomeDoPedido.textContent = gettemp()[0].produto

//botao que esta na page
const botaoConfirmaInsere = document.querySelector('#confirmaInsere')

botaoConfirmaInsere.addEventListener('click', (e) => {
    e.preventDefault()
    gettemp2().map((x) => criaPedidos(x.qtd, x.produto, x.preco, x.tipo, x.nomeDoBairro, x.nrDoPedido))
    apagatemp2()
    location.assign('./confirma.html')
})