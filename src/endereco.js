let endereco = []

const loadendereco = function(){
    const enderecoJSON = sessionStorage.getItem('endereco')
    
    if(enderecoJSON !== null){
        return JSON.parse(enderecoJSON)
    } else {
        return []
    }
}

const saveendereco = function(){
    sessionStorage.setItem('endereco', JSON.stringify(endereco))
}

//expose orders from module
const getendereco = () => endereco

//funcao para add endereco
const insereendereco = (cep, end, refe, bairro, cidade, n) => {
    endereco.unshift(cep, end, refe, bairro, cidade, n)
    saveendereco()
}

//funcao para limpar registro do cliente
const removeendereco = () => {
    sessionStorage.removeItem('endereco')
}

endereco = loadendereco()

export {getendereco, insereendereco, removeendereco, saveendereco}