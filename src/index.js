import { getCadastro, criaCadastro } from "./cadastro"
import { criaguardaCel, getguardaCel } from "./guardacel"
import { getPedidos, criaPedidos } from "./pedidos"
import { criaPedidosExclui, apagaPedidosExclui } from "./excluipedido"

const telInformado = document.querySelector('#telefoneCad')
telInformado.addEventListener('keypress', () =>{
    if(telInformado.value.length > 4 && telInformado.value.length < 6 && telInformado.value[0] == 9){
        telInformado.value = telInformado.value + '-'
    }
    if(telInformado.value[0] != 9 && telInformado.value.length > 3 && telInformado.value.length < 5){
        telInformado.value = '9' + telInformado.value + '-'
    }

})

const mudaPasso = () => {
    const formu = document.querySelector('#formularioParaSalvar')
    const passo1 = document.querySelector('#checaCadastro')
    passo1.setAttribute('style', 'display: none;')
    formu.removeAttribute('style')
}

telInformado.addEventListener('input', () => {
    if(telInformado.value.length == 10){
        
        if(getCadastro().length === 0){
            criaguardaCel(telInformado.value)
            if(getguardaCel().length > 0){
                const mostraoCelInformadoAntes = document.querySelector('#newTel')
                const showIndexLastCellInformed = getguardaCel().length - 1 
                mostraoCelInformadoAntes.value = getguardaCel()[showIndexLastCellInformed]
            }
            mudaPasso()
            
        }
        
    }
})

const pegaoCep = document.querySelector('#localizacep')
    pegaoCep.addEventListener('input', () => {
    const url = 'https://viacep.com.br/ws/' + pegaoCep.value.replace('-', '') +  '/json/'
    fetch(url)
        .then((r) => r.json())
        .then(data => {
            if(data.logradouro == undefined || data.bairro == undefined || data.localidade  == undefined || data.uf  == undefined){
                alert('Endereço inválido')
                const rua = document.querySelector('#endereco')
                rua.value = ''
                const bairr = document.querySelector('#bairro')
                bairr.value = ''
                const city = document.querySelector('#cidade')
                city.value = ''
            }else{
                const rua = document.querySelector('#endereco')
                rua.value = data.logradouro
                const bairr = document.querySelector('#bairro')
                const opt = document.querySelector('#dinamico')
                opt.textContent = data.bairro
                opt.setAttribute('value', `${data.bairro}`)
                bairr.appendChild(opt)
                bairr.value = data.bairro
                const city = document.querySelector('#cidade')
                city.value = data.localidade + ' - ' + data.uf 
            }
            
        })
        
})

const botaoDeLoginIndex = document.querySelector('#btnLogin')
botaoDeLoginIndex.addEventListener('click', (e) => {
    e.preventDefault()

    const usuario = document.querySelector('#name').value
    const fone = document.querySelector('#newTel').value
    const cepUser = document.querySelector('#localizacep').value
    const address = document.querySelector('#endereco').value
    const n = document.querySelector('#nro').value
    const pRefe = document.querySelector('#rfr').value
    const bairroUsr = document.querySelector('#bairro').value
    const cityUsr = document.querySelector('#cidade').value
    const retLoja = document.querySelector('#retiraNaLoja').checked

    if(retLoja === true){
        if(getPedidos().filter((x) => x.tipo === 'taxa').length == 0 && usuario !== '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''|| (getPedidos().filter((x) => x.tipo === 'taxa').length > 0 && getPedidos().filter((x) => x.produto.includes('Retirar')).length < 1) && usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''){
            criaPedidos(1, 'Retirar na loja', 0, 'taxa', '', 'Delivery')
            apagaPedidosExclui()
            // console.log('retira na loja')
        }
        
    }else{
        criaPedidosExclui('1')
    }

    if(getPedidos().length > 1 && usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''){
        criaCadastro(usuario, fone, cepUser, address, n, pRefe, bairroUsr, cityUsr)
        location.assign('./confirma.html')
        // console.log('cria cadastro e leva pro carrinho')
    }

    if(usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''  && getPedidos().length < 2){
        criaCadastro(usuario, fone, cepUser, address, n, pRefe, bairroUsr, cityUsr)
        // console.log('cria cadastro')
    }
    
    if(usuario == '' || fone == '' || cepUser == '' || address == '' || n == '' || pRefe == '' || bairroUsr == ''  && getPedidos().length < 2){
        alert('Preencha todos os campos')
        // console.log('preencha')
        
    }

    if(getPedidos().length < 2 && usuario != '' && fone != '' && cepUser != '' && address != '' && n != '' && pRefe != '' && bairroUsr != ''){
        location.reload()
        // console.log('reload')
    }

        
})

if(getCadastro().length > 0 && getguardaCel().length < 1 || getCadastro().length > 0 && getguardaCel().length > 0){
    const passo1 = document.querySelector('#checaCadastro') 
    passo1.setAttribute('style', 'display: none')
    const passo2 = document.querySelector('#formularioParaSalvar')
    passo2.setAttribute('style', 'display: none')
}