import {criatemp} from './temp'
//gera o menu na tela

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Sashimi Salmão - 5 peças',
    numero: '601',
    descricao: '(5 peças)',
    preco: 19.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Salmão - 10 peças',
    numero: '601',
    descricao: '(10 peças)',
    preco: 32.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Peixe Branco - 5 peças',
    numero: '603',
    descricao: '(5 peças)',
    preco: 19.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Peixe Branco - 10 peças',
    numero: '603',
    descricao: '(10 peças)',
    preco: 32.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Polvo - 5 peças',
    numero: '604',
    descricao: '(5 peças)',
    preco: 19.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Polvo - 10 peças',
    numero: '604',
    descricao: '(10 peças)',
    preco: 32.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Kani - 5 peças',
    numero: '605',
    descricao: '(5 peças)',
    preco: 19.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Kani - 10 peças',
    numero: '605',
    descricao: '(10 peças)',
    preco: 32.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Atum - 5 peças',
    numero: '602',
    descricao: '(5 peças)',
    preco: 19.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sashimi Atum - 10 peças',
    numero: '602',
    descricao: '(10 peças)',
    preco: 32.9,
    foto: '',
    tipo: 'sashimi'
}, {
    nomeHamburguer: 'Sunomono Atum',
    numero: '43',
    descricao: '',
    preco: 22.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Peixe Branco',
    numero: '44',
    descricao: '',
    preco: 21.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Kani',
    numero: '45',
    descricao: '',
    preco: 19.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Salmão',
    numero: '46',
    descricao: '',
    preco: 24.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Polvo',
    numero: '47',
    descricao: '',
    preco: 22.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Misto',
    numero: '48',
    descricao: '',
    preco: 24.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Peixe Frito',
    numero: '49',
    descricao: '',
    preco: 19.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Sunomono Pepino',
    numero: '50',
    descricao: '',
    preco: 12.9,
    foto: '',
    tipo: 'sunomono'
}, {
    nomeHamburguer: 'Ceviche',
    numero: '21',
    descricao: 'Iscas de fruto do mar, marinados com molho cítrico levemente apimentado',
    preco: 26.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Carpaccio de Salmão',
    numero: '22',
    descricao: 'Fatias finas de salmão temperadas ao natural',
    preco: 26.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Coni Ball (6 peças)',
    numero: '23',
    descricao: 'Camarão crocante e cream cheese envolto por salmão maçaricado e molho barbecue',
    preco: 22.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Eby Fly (2 peças)',
    numero: '24',
    descricao: 'Arroz envolto por lâmina de salmão recheados com cream cheese e camarão cozido',
    preco: 9.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Joy Salmão (2 peças)',
    numero: '25',
    descricao: 'Arroz envolto por lâmina de salmão recheado com patê de salmão e cream cheese',
    preco: 9.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Eby Ball (10 peças)',
    numero: '26',
    descricao: 'Bolinhas de camarão crocante e cream cheese com molhos: Mostarda e Mel | Geleia de pimenta | Molho da casa | Barbecue',
    preco: 19.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Kani Cheese',
    numero: '27',
    descricao: 'Bolinhas de kani e cream cheese',
    preco: 14.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Tempurá de Peixe',
    numero: '29',
    descricao: 'Tiras de peixe crocante e e molhos a sua escolha',
    preco: 19.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Bastão de Salmão (8 unidades)',
    numero: '31',
    descricao: 'Bastão de massa harumaki recheado com salmão temperado e cream cheese',
    preco: 19.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Bastão de Camarão (8 unidades)',
    numero: '32',
    descricao: 'Bastão de massa harumaki recheado com camarão temperado e cream cheese',
    preco: 22.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Rolinho Primavera (2 unidades)',
    numero: '33',
    descricao: 'Rolinho de harumaki recheado com carne e legumes',
    preco: 10.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Espetinho de Camarão com Queijo',
    numero: '34',
    descricao: 'Camarão empanado com queijo coalho e molho rosé ou geléia de pimenta',
    preco: 16.9,
    foto: '',
    tipo: 'entradas'
}, {
    nomeHamburguer: 'Skin Philadelphia',
    numero: '60',
    descricao: 'Pele de salmão crocante e com cream cheese e cebolinho',
    preco: 13.9,
    foto: '',
    tipo: 'temakisquentes'
}, {
    nomeHamburguer: 'Peixe Branco Maçaricado',
    numero: '61',
    descricao: 'Peixe branco maçaricado com barbecue',
    preco: 17.9,
    foto: '',
    tipo: 'temakisquentes'
}, {
    nomeHamburguer: 'Eby Philadelphia',
    numero: '62',
    descricao: 'Camarão crocante com cream cheese',
    preco: 18.9,
    foto: '',
    tipo: 'temakisquentes'
}//, {
    // nomeHamburguer: 'Eby Cheddar',
    // numero: '63',
    // descricao: 'Camarão crocante com cheddar',
    // preco: 17.9,
    // foto: '',
    // tipo: 'temakisquentes'
//}
, {
    nomeHamburguer: 'Hot Philadelphia',
    numero: '64',
    descricao: 'Salmão e cream cheese crocante',
    preco: 18.9,
    foto: '',
    tipo: 'temakisquentes'
}//, {
//     nomeHamburguer: 'Cheddar Snake Ball',
//     numero: '65',
//     descricao: 'Pedaços de salmão crocante e cheddar',
//     preco: 19.9,
//     foto: '',
//     tipo: 'temakisquentes'
// }
, {
    nomeHamburguer: 'Polvo ao Barbecue',
    numero: '74',
    descricao: 'Tirinhas de polvo frito com molho barbecue',
    preco: 19.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Kani Crock',
    numero: '75',
    descricao: 'Pedaços de kani crocante com cream cheese',
    preco: 16.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Sherelete',
    numero: '76',
    descricao: 'Fatias de peixe branco empanado, puxado no teriyaki com gergelim',
    preco: 18.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Hot Peixe Branco',
    numero: '77',
    descricao: 'Cubos de peixe branco maçaricados com teriyaki',
    preco: 18.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Spring',
    numero: '78',
    descricao: 'Sunômono, morango e manga',
    preco: 14.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Eby Hot',
    numero: '79',
    descricao: 'Camarão cozido com geleia de pimenta e pimenta biquinho',
    preco: 21.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Eby Barbecue',
    numero: '80',
    descricao: 'Camarão cozido com molho barbecue',
    preco: 21.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Tropical Shake',
    numero: '82',
    descricao: 'Salmão em cubos com pedaços de morango, manga, abacaxi e cream cheese',
    preco: 19.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Kappa Shake',
    numero: '83',
    descricao: 'Salmão em cubos e sunomôno',
    preco: 19.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Salmão Philadelphia Crisps',
    numero: '84',
    descricao: 'Cubos de salmão philadelphia com flocos de arroz',
    preco: 21.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Coni Ball',
    numero: '86',
    descricao: 'Camarão crocante e cream cheese envolto por salmão maçaricado e molho barbecue',
    preco: 21.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Eby Shake',
    numero: '87',
    descricao: 'Salmão philadelphia com camarões crocantes',
    preco: 19.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Peixe Crocante',
    numero: '88',
    descricao: 'Tiras de peixe crocante com molho barbecue',
    preco: 16.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Ceviche',
    numero: '89',
    descricao: 'Iscas de frutos do mar, marinados com molho cítrico levemente apimentado',
    preco: 21.9,
    foto: '',
    tipo: 'temakisdochef'
}, {
    nomeHamburguer: 'Salmão Supremo com Camarão',
    numero: '90',
    descricao: 'Cone gigante de manta de salmão maçaricado, recheio com camarão crocante e cream cheese',
    preco: 45.9,
    foto: '',
    tipo: 'salmaosupremo'
}, {
    nomeHamburguer: 'Salmão Supremo sem camarão',
    numero: '91',
    descricao: 'Cone gigante de manta de salmão maçaricado, com recheio de skin ou kani ou peixe crocante e cream cheese',
    preco: 41.9,
    foto: '',
    tipo: 'salmaosupremo'
}, {
    nomeHamburguer: 'Salmão',
    numero: '95',
    descricao: 'Salmão com gergelim e cebolinho',
    preco: 21.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Salmão Philadelphia',
    numero: '96',
    descricao: 'Cubos de salmão com cream cheese e cebolinho',
    preco: 21.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Spice Salmon',
    numero: '97',
    descricao: 'Cubos de salmão com geleia de pimenta e pimenta biquinho',
    preco: 21.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Skin Shake',
    numero: '98',
    descricao: 'Salmão philadelphia com fritas de pele de salmão crocante',
    preco: 17.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Kani',
    numero: '99',
    descricao: 'Cubos de kani kama',
    preco: 15.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Kani Philadelphia',
    numero: '100',
    descricao: 'Cubos de kani com cream cheese e cebolinho',
    preco: 16.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Kani Shake',
    numero: '101',
    descricao: 'Salmão philadelphia com cubos de kani',
    preco: 19.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Polvo Philadelphia',
    numero: '102',
    descricao: 'Lascas de polvo e cream cheese',
    preco: 19.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Califórnia',
    numero: '103',
    descricao: 'Kani, manga e pepino',
    preco: 16.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Peixe Branco Philadelphia',
    numero: '104',
    descricao: 'Cubos de peixe branco com cream cheese',
    preco: 18.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Vegano Califórnia',
    numero: '105',
    descricao: 'Morango, manga e pepino',
    preco: 16.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Camarão Philadelphia',
    numero: '106',
    descricao: 'Camarão cozido e cream cheese',
    preco: 21.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Eby Shake Natural',
    numero: '107',
    descricao: 'Salmão philadelphia com camarões cozidos',
    preco: 21.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Atum',
    numero: '108',
    descricao: 'Atum com gergelim e cebolinho',
    preco: 19.9,
    foto: '',
    tipo: 'temakisfrios'
}, {
    nomeHamburguer: 'Shake Maki',
    numero: '112',
    descricao: 'Salmão, arroz e alga por fora',
    preco: 17.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Teka Maki',
    numero: '113',
    descricao: 'Atum, arroz e alga por fora',
    preco: 17.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Kani Maki',
    numero: '114',
    descricao: 'Kani, arroz e alga por fora',
    preco: 15.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Salmão Philadelphia',
    numero: '115',
    descricao: 'Salmão e cream cheese',
    preco: 21.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Kani Philadelphia',
    numero: '116',
    descricao: 'Kani e cream cheese',
    preco: 16.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Tempura Roll',
    numero: '117',
    descricao: 'Camarão empanado e cream cheese',
    preco: 17.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'California',
    numero: '118',
    descricao: 'Kani, manga e pepino',
    preco: 15.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Skin Philadelphia',
    numero: '119',
    descricao: 'Pele de salmão crocante e cream cheese',
    preco: 15.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Morango Philadelphia',
    numero: '120',
    descricao: 'Pedaços de morango e cream cheese',
    preco: 15.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Vegano California',
    numero: '121',
    descricao: 'Morango, manga e pepino',
    preco: 15.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Carioca',
    numero: '122',
    descricao: 'Salmão e cream cheese empanado',
    preco: 21.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Hot Special',
    numero: '123',
    descricao: 'Salmão, camarão, kani e cream cheese envoltos em massa de harumaki',
    preco: 24.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Kani Special',
    numero: '124',
    descricao: 'Kani philadelphia coberto com fatias de salmão, atum e peixe branco',
    preco: 24.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Sherelete',
    numero: '125',
    descricao: 'Fatias de peixe branco empanado com teriaki e gergilim no recheio e no topo',
    preco: 21.9,
    foto: '',
    tipo: 'rolls'
}, {
    nomeHamburguer: 'Choco Balls',
    numero: '131',
    descricao: 'Coni crocante com chocolate e bolinhas de chocolate',
    preco: 9.9,
    foto: '',
    tipo: 'desserts'
}, {
    nomeHamburguer: 'Choco Mix',
    numero: '132',
    descricao: 'Coni crocante com chocolate e frutas da época',
    preco: 9.9,
    foto: '',
    tipo: 'desserts'
}, {
    nomeHamburguer: 'Rolinho Romeu e Julieta',
    numero: '133',
    descricao: 'Rolinho de karumaki recheado com queijo e goiabada',
    preco: 9.9,
    foto: '',
    tipo: 'desserts'
}, {
    nomeHamburguer: 'Beijinho',
    numero: '134',
    descricao: 'Coni crocante recheado com beijinho e castanha',
    preco: 9.9,
    foto: '',
    tipo: 'desserts'
}, {
    nomeHamburguer: 'Danado de bom',
    numero: '135',
    descricao: 'Bolo de rolo na chapa e sorvete de cream cheese poleguinho com calda de goiaba',
    preco: 18.9,
    foto: '',
    tipo: 'desserts'
}, {
    nomeHamburguer: 'Brownie com Sorvete',
    numero: '136',
    descricao: 'Brownie com sorvete de creme e calda de chocolate',
    preco: 18.9,
    foto: '',
    tipo: 'desserts'
}, {
    nomeHamburguer: 'Promoção 01',
    numero: '801',
    descricao: '01 Coni de Skin + 10 unidades Roll de Skin (1 refri lata 350ml grátis)',
    preco: 25.8,
    foto: '',
    tipo: 'promocaoMini'
}, {
    nomeHamburguer: 'Promoção 02',
    numero: '802',
    descricao: '01 Coni de Hot Philadelphia + 10 unidades Roll de Carioca (1 refri lata 350ml grátis)',
    preco: 38.8,
    foto: '',
    tipo: 'promocaoMini'
}, {
    nomeHamburguer: 'Promoção 03',
    numero: '803',
    descricao: '01 Coni de Eby Philadelphia + 10 unidades de Tempura Roll (1 refri lata 350ml grátis)',
    preco: 33.8,
    foto: '',
    tipo: 'promocaoMini'
}, {
    nomeHamburguer: 'Promoção 04',
    numero: '804',
    descricao: '01 Coni de Kani Philadelphia + 10 unidades Roll de Kani Philadelphia (1 refri lata 350ml grátis)',
    preco: 29.8,
    foto: '',
    tipo: 'promocaoMini'
}, {
    nomeHamburguer: 'Combo Master 01 ',
    numero: '810',
    descricao: '5 Rolls Kani Maki, 5 Rolls Kani, 5 Rolls Salmão, 5 Tempura Rolls, 5 Rolls Skin, 5 Rolls Shake Maki (1 refri de 500ml grátis)',
    preco: 49.9,
    foto: '',
    tipo: 'promocaoCombo'
}, {
    nomeHamburguer: 'Combo Master 02',
    numero: '811',
    descricao: '10 Niguiri Salmão, 5 Niguiri Polvo, 5 Niguiri Skin, 5 Niguiri Kani, 5 Niguiri Peixe Branco, 5 Tempura Rolls, 5 Roll de Skin (1 refri de 1L grátis)',
    preco: 69.9,
    foto: '',
    tipo: 'promocaoCombo'
}, {
    nomeHamburguer: 'Combo Master 03',
    numero: '812',
    descricao: '10 Rolls Shake Maki, 10 Roll Kani Maki, 5 Coni Ball, 5 Joy Salmão, 5 Edy Fly, 10 Niguiri de Salmão, 10 Rolls de Salmão Philadelphia e 10 Niguiri de Polvo (1 refri de 1L grátis)',
    preco: 139.9,
    foto: '',
    tipo: 'promocaoCombo'
}]

//os adicionais ficam em molhos e recheios

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    // if(lanche.tipo === 'saladaverde'){

    // }else{
    //     divMain.appendChild(imagem)
    // }
    select.setAttribute('style', 'grid-column: 1;')
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')     
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipo = lanche.tipo
        const numero = lanche.numero
        criatemp(qt, prodt, price, tipo, '', numero)
        location.assign('./conftemp.html')
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo === 'sashimi').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'sunomono').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#sunomonoDom').appendChild(hrEl)
        document.querySelector('#sunomonoDom').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'entradas').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#entradasDom').appendChild(hrEl)
        document.querySelector('#entradasDom').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'temakisquentes').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#temakisquentesDom').appendChild(hrEl)
        document.querySelector('#temakisquentesDom').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'temakisdochef').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#temakisdochefDom').appendChild(hrEl)
        document.querySelector('#temakisdochefDom').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'salmaosupremo').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#temakisdochefDom').appendChild(hrEl)
        document.querySelector('#temakisdochefDom').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'temakisfrios').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#temakisfriosDom').appendChild(hrEl)
        document.querySelector('#temakisfriosDom').appendChild(addElements(lanche))
    })
    cardapio.filter((x) => x.tipo === 'rolls').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#rollsDom').appendChild(hrEl)
        document.querySelector('#rollsDom').appendChild(addElements(lanche))
    })
    
    
}

const addCardapioSobremesas = () => {
    cardapio.filter((x) => x.tipo === 'desserts').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#dessertsDom').appendChild(hrEl)
        document.querySelector('#dessertsDom').appendChild(addElements(lanche))
    })
}

const addCardapioPromocoesMini = () => {
    cardapio.filter((x) => x.tipo === 'promocaoMini').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#promoDom').appendChild(hrEl)
        document.querySelector('#promoDom').appendChild(addElements(lanche))
    })
}

const addCardapioCombosMaster = () => {
    cardapio.filter((x) => x.tipo === 'promocaoCombo').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#ComboDom').appendChild(hrEl)
        document.querySelector('#ComboDom').appendChild(addElements(lanche))
    })
}


export {addCardapio, addCardapioSobremesas, addElements, addCardapioPromocoesMini, addCardapioCombosMaster}