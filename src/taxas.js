import {criaPedidos, getPedidos} from './pedidos'

const pulaTaxa = () => {
    if(getPedidos().map((x) => x.tipo).includes('taxa')){
        location.assign('./confirma.html')
    }
}

const addTblEl = function (){
    const tblEl = document.createElement('table')
    document.querySelector('#txBairro').appendChild(tblEl)
    
    //header da tabela
    
    const qtyEl = document.createElement('th')
    qtyEl.textContent = ''
    document.querySelector('table').appendChild(qtyEl)
    
    const district = document.createElement('th')
    district.textContent = 'BAIRRO'
    document.querySelector('table').appendChild(district)
    
    const priceEl = document.createElement('th')
    priceEl.textContent = 'TAXA DE ENTREGA'
    document.querySelector('table').appendChild(priceEl)
    
    const solicitaTx = document.createElement('th')
    solicitaTx.textContent = 'PEDIR'
    document.querySelector('table').appendChild(solicitaTx)
}

//gera o menu na tabela

const bairrosLista = [{
    nomeBairro: 'Retirar na loja',
    valor: 0,
    taxa: 'taxa',
    formBairro: 'Retirar na loja'
},{
    nomeBairro: 'Boa Vista',
    valor: 4.9,
    taxa: 'taxa',
    formBairro: 'Boa Vista'
},{
    nomeBairro: 'Derby',
    valor: 5.9,
    taxa: 'taxa',
    formBairro: 'Derby'
},{
    nomeBairro: 'Ilha do Leite',
    valor: 5.9,
    taxa: 'taxa',
    formBairro: 'Ilha do Leite'
},{
    nomeBairro: 'Ilha do Retiro',
    valor: 6,
    taxa: 'taxa',
    formBairro: 'Ilha do Retiro'
},{
    nomeBairro: 'Santo Antônio',
    valor: 5.9,
    taxa: 'taxa',
    formBairro: 'Santo Antônio'
},{
    nomeBairro: 'Afogados',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Afogados'
},{
    nomeBairro: 'Prado',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Prado'
},{
    nomeBairro: 'Bongi',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Bongi'
},{
    nomeBairro: 'San Martin',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'San Martin'
},{
    nomeBairro: 'Torrões',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Torrões'
},{
    nomeBairro: 'Madalena',
    valor: 7.9,
    taxa: 'taxa',
    formBairro: 'Madalena'
},{
    nomeBairro: 'Graças',
    valor: 6.9,
    taxa: 'taxa',
    formBairro: 'Graças'
},{
    nomeBairro: 'Espinheiro',
    valor: 6.9,
    taxa: 'taxa',
    formBairro: 'Espinheiro'
},{
    nomeBairro: 'Encruzilhada',
    valor: 7.9,
    taxa: 'taxa',
    formBairro: 'Encruzilhada'
},{
    nomeBairro: 'Casa Amarela',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Casa Amarela'
},{
    nomeBairro: 'Parnamirim',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Parnamirim'
},{
    nomeBairro: 'Casa Forte',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Casa Forte'
},{
    nomeBairro: 'Sítio Novo',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Sítio Novo'
},{
    nomeBairro: 'Brasília Teimosa',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Brasília Teimosa'
},{
    nomeBairro: 'Pina',
    valor: 8.9,
    taxa: 'taxa',
    formBairro: 'Pina'
},{
    nomeBairro: 'Imbiribeira',
    valor: 9.9,
    taxa: 'taxa',
    formBairro: 'Imbiribeira'
},{
    nomeBairro: 'Ipsep',
    valor: 9.9,
    taxa: 'taxa',
    formBairro: 'Ipsep'
},{
    nomeBairro: 'Boa Viagem',
    valor: 9.9,
    taxa: 'taxa',
    formBairro: 'Boa Viagem'
},{
    nomeBairro: 'Várzea',
    valor: 9.9,
    taxa: 'taxa',
    formBairro: 'Várzea'
},{
    nomeBairro: 'Caxangá',
    valor: 7.9,
    taxa: 'taxa',
    formBairro: 'Caxangá'
},{
    nomeBairro: 'Arruda',
    valor: 7.9,
    taxa: 'taxa',
    formBairro: 'Arruda'
},{
    nomeBairro: 'Outros bairros (a taxa será informada no final)',
    valor: 7.9,
    taxa: 'taxa',
    formBairro: 'Outros Bairros'
}]

const generateDomDistrict = (bairro) => {
    const bairroEl = document.createElement('tr')
    const inputH = document.createElement('input')
    const tdGeral = document.createElement('td')
    const tdBairro = document.createElement('td')
    const tdValor = document.createElement('td')
    const tdBtn = document.createElement('td')
    const btn = document.createElement('button')

    bairroEl.appendChild(tdGeral)
    tdGeral.appendChild(inputH)
    inputH.setAttribute('type', 'hidden')
    inputH.setAttribute('value', '1')

    bairroEl.appendChild(tdBairro)
    tdBairro.textContent = bairro.nomeBairro

    bairroEl.appendChild(tdValor)
    tdValor.textContent = 'R$ ' + bairro.valor.toFixed(2).replace('.', ',')

    bairroEl.appendChild(tdBtn)
    btn.textContent = 'ESCOLHER'
    tdBtn.appendChild(btn)

    btn.addEventListener('click', (e) => {
        e.preventDefault()
        if(bairro.nomeBairro === 'Retirar na loja' || bairro.nomeBairro === 'Outros bairros (a taxa será informada no final)') {
            criaPedidos(parseFloat(inputH.value), bairro.nomeBairro, bairro.valor, bairro.taxa, bairro.formBairro, 'Delivery')
        } else{
            criaPedidos(parseFloat(inputH.value), ('TAXA DE ENTREGA ' + bairro.nomeBairro), bairro.valor, bairro.taxa, bairro.formBairro, 'Delivery')
        }

        location.assign('./confirma.html')
    })

    return bairroEl

}

const addBairrosTela = () => {
    bairrosLista.forEach((bairro) => {
        document.querySelector('table').appendChild(generateDomDistrict(bairro))
    })
}

export {addTblEl, addBairrosTela, generateDomDistrict, pulaTaxa}