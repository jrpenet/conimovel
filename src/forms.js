import {insereClientes} from './cliente'
import {getPedidos} from './pedidos'
import {getendereco} from './endereco'
import {getClientes} from './cliente'

//refresh automatico pra evitar bugs

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

 if(getClientes().length > 0){
   location.assign('./confcli.html')
}

if(getPedidos().map((x)=> x.tipo).includes('taxa')){
   
}else{
   location.assign('./txentrega2.html')
}

const endereco = document.querySelector('#endereco')
endereco.textContent = getendereco()[1]
const nro = document.querySelector('#nro')
nro.textContent = getendereco()[5]
const bairroNoForm = document.querySelector('#bairro')
if(getendereco()[3] === ''){
   bairroNoForm.textContent = 'Outros Bairros'
}else{
   bairroNoForm.textContent = getendereco()[3]
}


const pr = document.querySelector('#pto_referencia')
pr.textContent = getendereco()[2]

if(getPedidos().filter((x) => x.produto === 'Retirada na loja').length > 0){
   endereco.textContent = ''
   bairroNoForm.textContent = 'Retirada na loja'
   pr.textContent = ''
   nro.textContent = ''
}

document.querySelector('#confirmarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    const nome = document.querySelector('#nome').value
    const cel = document.querySelector('#celular').value
    const pgt = document.querySelector('#pagaEM').value
    const troco = document.querySelector('#troco').value
    const obs = document.querySelector('#observacao').value

   if(nome === '' || cel === ''){ //bairro === '' || 
      alert('Os campos nome e celular são obrigatórios')
   }else {
      insereClientes(nome, getendereco()[0], getendereco()[1], getendereco()[3], getendereco()[2], cel, pgt, troco, obs, getendereco()[5], getendereco()[4])
      location.assign('./enviapedidos.html')
 }

   if(getPedidos().filter((x) => x.produto === 'Retirada na loja').length > 0){
      insereClientes(nome, '', '', 'Retirada na loja', '', cel, pgt, troco, obs, '', getendereco()[4])
   }

})
