let pedidosExclui = []

const loadPedidosExclui = function(){
    const pedidosExcluiJSON = sessionStorage.getItem('pedidosExclui')
    
    if(pedidosExcluiJSON !== null){
        return JSON.parse(pedidosExcluiJSON)
    } else {
        return []
    }
}

const savePedidosExclui = function(){
    sessionStorage.setItem('pedidosExclui', JSON.stringify(pedidosExclui))
}

//expose orders from module
const getPedidosExclui = () => pedidosExclui

const criaPedidosExclui = (select) =>{
    
    pedidosExclui.push({
        excluir: select
    })
    savePedidosExclui()
}

const apagaPedidosExclui = () => {
    sessionStorage.removeItem('pedidosExclui')
}

const removePedidosExclui = (item) => {
    pedidosExclui.splice(item, 1)
    savePedidosExclui()
    window.location.reload()
}

pedidosExclui = loadPedidosExclui()

export { getPedidosExclui, criaPedidosExclui, savePedidosExclui, removePedidosExclui, apagaPedidosExclui}  