import {addCardapioPromocoesMini, addCardapioCombosMaster} from './menuHam'
import {apagatemp} from './temp'
import {getPedidos} from './pedidos'

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

addCardapioPromocoesMini()
addCardapioCombosMaster()
apagatemp()

if(getPedidos().filter((x) => x.tipo === 'promocaoMini').length > 0 || getPedidos().filter((x) => x.tipo === 'promocaoCombo').length > 0) {
   const informa = document.querySelector('#msgCart')
   informa.textContent = 'Para fechar o pedido clique aqui!!!'
   informa.setAttribute('class', 'redirectCartMSG')
   informa.addEventListener('click', () => {
      location.assign('./confirma.html')
   })

   const informa2 = document.querySelector('#msgCart2')
   informa2.textContent = 'Para fechar o pedido clique aqui!!!'
   informa2.setAttribute('class', 'redirectCartMSG')
   informa2.addEventListener('click', () => {
      location.assign('./confirma.html')
   })

   const informa3 = document.querySelector('#msgCart3')
   informa3.textContent = 'Para fechar o pedido clique aqui!!!'
   informa3.setAttribute('class', 'redirectCartMSG')
   informa3.addEventListener('click', () => {
      location.assign('./confirma.html')
   })

}

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})