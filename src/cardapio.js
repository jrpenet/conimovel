import {addCardapio} from './menuHam'
import {apagatemp} from './temp'

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

addCardapio()
apagatemp()

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})