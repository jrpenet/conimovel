import { gettemp} from './temp'
import { criatemp2, removetemp2} from './temp2'

let temp2 = []

const loadtemp2 = function(){
    const temp2JSON = sessionStorage.getItem('temp2')
    
    if(temp2JSON !== null){
        return JSON.parse(temp2JSON)
    } else {
        return []
    }
}

const molhosEBY = [{
    nomeMolho: 'Molho da casa',
    codigo: 'molhodacasa',
    numero: '501',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
},{
    nomeMolho: 'Geleia de Pimenta',
    codigo: 'geleiadepimenta',
    numero: '216',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
},{
    nomeMolho: 'Mostarda e mel',
    codigo: 'mostardaemel',
    numero: '503',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
},{
    nomeMolho: 'Barbecue',
    codigo: 'barbecue',
    numero: '999',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
}]

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('span')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('input')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeMolho
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    
    //btn.textContent = 'PEDIR'
    if(lanche.nomeMolho === 'Molho da casa'){
        btn.setAttribute('checked', '')
    }
    btn.setAttribute('type', 'radio')
    btn.setAttribute('name', 'optMolhos')
    btn.setAttribute('id', `chkMolho${lanche.codigo}`)
    btn.setAttribute('value', `${lanche.nomeMolho}`)
    
    divMain.appendChild(btn)
    divMain.appendChild(titulo)

    return divMain
}

const addCardapioMolho = () => {
    molhosEBY.forEach((lanche) => {
        document.querySelector('#molhosRecheios').appendChild(addElements(lanche))
    })
}

const molhosEspetinho = [{
    nomeMolho: 'Rosé',
    codigo: 'rose',
    numero: '502',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
},{
    nomeMolho: 'Geleia de Pimenta',
    codigo: 'geleiadepimenta',
    numero: '216',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
}]

const addElementsEspetinho = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('span')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('input')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeMolho
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    
    //btn.textContent = 'PEDIR'
    if(lanche.nomeMolho === 'Rosé'){
        btn.setAttribute('checked', '')
    }
    btn.setAttribute('type', 'radio')
    btn.setAttribute('name', 'optMolhos')
    btn.setAttribute('id', `chkMolho${lanche.codigo}`)
    btn.setAttribute('value', `${lanche.nomeMolho}`)
    
    divMain.appendChild(btn)
    divMain.appendChild(titulo)

    return divMain
}

const addCardapioMolhoEspetinho = () => {
    molhosEspetinho.forEach((lanche) => {
        document.querySelector('#molhosRecheios').appendChild(addElementsEspetinho(lanche))
    })
}

const recheiosSalmao = [{
    nomeMolho: 'Skin',
    codigo: 'skin',
    numero: '999',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
},{
    nomeMolho: 'Kani',
    codigo: 'kani',
    numero: '205',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
},{
    nomeMolho: 'Peixe Crocante',
    codigo: 'peixecrocante',
    numero: '999',
    descricao: '',
    preco: 0,
    foto: '',
    tipo: 'molho'
}]

const addElementsRecheios = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('span')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('input')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeMolho
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    
    //btn.textContent = 'PEDIR'
    if(lanche.nomeMolho === 'Skin'){
        btn.setAttribute('checked', '')
    }
    btn.setAttribute('type', 'radio')
    btn.setAttribute('name', 'optMolhos')
    btn.setAttribute('id', `chkMolho${lanche.codigo}`)
    btn.setAttribute('value', `${lanche.nomeMolho}`)
    
    divMain.appendChild(btn)
    divMain.appendChild(titulo)

    return divMain
}

const addCardapioRecheios = () => {
    recheiosSalmao.forEach((lanche) => {
        document.querySelector('#molhosRecheios').appendChild(addElementsRecheios(lanche))
    })
}

//adicionais

const cardapio = [ {
    nomeHamburguer: 'Kit Viagem P',
    numero: '301',
    descricao: 'Embalagem, shoyo e hashi',
    preco: 2,
    foto: '',
    tipo: 'adicionais'
},{
    nomeHamburguer: 'Kit Viagem G',
    numero: '302',
    descricao: 'Embalagem, shoyo e hashi',
    preco: 4,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Maçaricar',
    numero: '303',
    descricao: 'Maçaricar alguns recheios',
    preco: 1.5,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Empanar',
    numero: '304',
    descricao: 'Empanar alguns recheios',
    preco: 2,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Teriyaki',
    numero: '305',
    descricao: '',
    preco: 1.5,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Temaki sem Arroz',
    numero: '306',
    descricao: 'Recheio completo sem arroz',
    preco: 9,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Porção Extra',
    numero: '307',
    descricao: '',
    preco: 1.5,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Porção de Shari',
    numero: '308',
    descricao: 'Arroz',
    preco: 7.9,
    foto: '',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Sumo de Limão',
    numero: '400',
    descricao: '',
    preco: 2,
    foto: '',
    tipo: 'adicionais'
}]

const addElementsAdicionais = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'PEDIR'
    //btn.setAttribute('type', 'checkbox')
    btn.setAttribute('class', 'btnPedir')
    //btn.setAttribute('name', 'adds')

    
    
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    // if(lanche.tipo === 'saladaverde'){

    // }else{
    //     divMain.appendChild(imagem)
    // }
    subValor.setAttribute('style', 'grid-column: 1;')
    //divMain.appendChild(select)
    //select.appendChild(opt)
    //select.appendChild(opt2)
    //select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')     
    })
    
    btn.addEventListener('click', () => {
        //const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tipo = lanche.tipo
        const numero = lanche.numero
        criatemp2(1, 'Adicional: ' + prodt + ` (${gettemp()[0].produto})`, price, tipo, '', numero)
        
        location.reload()
       
    })

    return divMain
}


const addCardapioAdicionais = () => {
    const h2 = document.createElement('h2')
    h2.textContent = 'Adicionais'
    document.querySelector('#adicionaisDom').appendChild(h2)


    cardapio.filter((x) => x.tipo === 'adicionais').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#adicionaisDom').appendChild(hrEl)
        document.querySelector('#adicionaisDom').appendChild(addElementsAdicionais(lanche))
    })
}


const addElementsDinamics = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    // const imagem = document.createElement('img')
    const select = document.createElement('select')
    // const opt = document.createElement('option')
    // const opt2 = document.createElement('option')
    // const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.produto
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    // imagem.setAttribute('src', lanche.foto)
    // imagem.setAttribute('class', 'hamburguerFoto')
    // opt.textContent = 1
    // opt.setAttribute('value', '1')
    // opt2.textContent = 2
    // opt2.setAttribute('value', '2')
    // opt3.textContent = 3
    // opt3.setAttribute('value', '3')
    // subValor.setAttribute('class', 'subtotalValor')
    // subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'Remover'
    //btn.setAttribute('type', 'checkbox')
    btn.setAttribute('class', 'btnPedir')
       
    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    subValor.setAttribute('style', 'grid-column: 1;')
    //divMain.appendChild(select)
    //select.appendChild(opt)
    //select.appendChild(opt2)
    //select.appendChild(opt3)
    divMain.appendChild(subValor)
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        const abc = e.target.value * lanche.preco
        subValor.textContent = 'R$ ' + abc.toFixed(2).replace('.', ',')     
    })
    
    btn.addEventListener('click', () => {
        const a = temp2.indexOf(lanche)
        removetemp2(a) 
        location.reload()      
    })

    return divMain
}


const addCardapioDinamics = () => {
    const h2 = document.createElement('h2')
    h2.textContent = 'Acompanha:'
    document.querySelector('#listaDin').appendChild(h2)
    console.log('temp2')

    temp2.filter((x) => x.tipo === 'adicionais').forEach((lanche) => {
        document.querySelector('#listaDin').appendChild(addElementsDinamics(lanche))
    })
}

temp2 = loadtemp2()


export {addCardapioMolho, addCardapioMolhoEspetinho, addCardapioRecheios, addCardapioAdicionais, addCardapioDinamics}