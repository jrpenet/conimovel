import {getPedidos, criaPedidos, removePedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'
import { apagatemp } from './temp'
import {getCadastro} from './cadastro'
import { getPedidosExclui, criaPedidosExclui } from './excluipedido'

cartTable()
insereCart()
rodapeCart()
apagatemp()


if(getPedidos().filter((x) => x.tipo === 'taxa').length < 1 && getCadastro().length > 0){
    //console.log('add bairro')
    const bairr = getCadastro().map((x) => x.nomeDoBairro)[0]
    if(bairr == 'Boa Vista' || bairr == 'Soledade' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 4.9, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Derby' || bairr == 'Ilha do Leite' || bairr == 'Santo Antônio' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 5.9, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Ilha do Retiro' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 6, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Afogados' || bairr == 'Prado' || bairr == 'Bongi' || bairr == 'San Martin' || bairr == 'Torrões' || bairr == 'Casa Amarela' || bairr == 'Parnamirim' || bairr == 'Casa Forte' || bairr == 'Sítio Novo' || bairr == 'Brasília Teimosa' || bairr == 'Pina' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 8.9, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Madalena' || bairr == 'Encruzilhada' || bairr == 'Caxangá' || bairr == 'Arruda' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 7.9, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Graças' || bairr == 'Espinheiro' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 6.9, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr == 'Imbiribeira' || bairr == 'Ipsep'|| bairr == "Caixa D'Água" || bairr == 'Boa Viagem' || bairr == 'Várzea' ){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 9.9, 'taxa', bairr, 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }else if(bairr != 'Boa Vista' && bairr != 'Derby' && bairr != 'Ilha do Leite' && bairr != 'Santo Antônio' && bairr != 'Ilha do Retiro' && bairr != 'Afogados' && bairr != 'Prado' && bairr != 'Bongi' && bairr != 'San Martin' && bairr != 'Torrões' && bairr != 'Casa Amarela' && bairr != 'Parnamirim' && bairr != 'Casa Forte' && bairr != 'Sítio Novo' && bairr != 'Brasília Teimosa' && bairr != 'Pina' && bairr != 'Madalena' && bairr != 'Encruzilhada' && bairr != 'Caxangá' && bairr != 'Arruda' && bairr != 'Graças' && bairr != 'Espinheiro' && bairr != 'Imbiribeira' && bairr != 'Ipsep' && bairr != 'Boa Viagem' && bairr != 'Várzea'){
        criaPedidos(1, 'Outros bairros - a taxa será informada no final' + '(' + bairr + ')', 0, 'taxa', 'Outros Bairros', 'Delivery')
        criaPedidosExclui(1)
        location.reload()
    }
}

document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    if(getPedidos().map((x)=> x.tipo).includes('taxa')){
        location.assign('./confcli.html')
    }
})