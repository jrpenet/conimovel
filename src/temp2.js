let temp2 = []

const loadtemp2 = function(){
    const temp2JSON = sessionStorage.getItem('temp2')
    
    if(temp2JSON !== null){
        return JSON.parse(temp2JSON)
    } else {
        return []
    }
}

const savetemp2 = function(){
    sessionStorage.setItem('temp2', JSON.stringify(temp2))
}

//expose orders from module
const gettemp2 = () => temp2

const criatemp2 = (select, hamb, precoProduto, tx, bairro, nr) =>{
    
    temp2.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        tipo: tx,
        nomeDoBairro: bairro,
        nrDoPedido: nr
    })
    savetemp2()
}

const removetemp2 = (item) => {
    temp2.splice(item, 1)
    savetemp2()
}

const apagatemp2 = () => sessionStorage.removeItem('temp2')

temp2 = loadtemp2()

export { gettemp2, criatemp2, savetemp2, removetemp2, apagatemp2}