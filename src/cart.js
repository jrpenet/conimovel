import {getPedidos, removePedidos, removePedidos1} from './pedidos'
import { getCadastro } from './cadastro'
import { getPedidosExclui, apagaPedidosExclui, criaPedidosExclui } from './excluipedido'

let pedidos = []

const loadPedidos = function(){
    const pedidosJSON = sessionStorage.getItem('pedidos')
    
    if(pedidosJSON !== null){
        return JSON.parse(pedidosJSON)
    } else {
        return []
    }
}

const cartTable = () => {
    const tabela3 = document.createElement('table')
    document.querySelector('#confirmacao').appendChild(tabela3)

    //header da tabela

    const quantid = document.createElement('th')
    quantid.textContent = 'QTD'
    document.querySelector('table').appendChild(quantid)

    const refere = document.createElement('th')
    refere.textContent = 'ITEM'
    document.querySelector('table').appendChild(refere)

    const unit = document.createElement('th')
    unit.textContent = 'PREÇO'
    document.querySelector('table').appendChild(unit)

    const subt = document.createElement('th')
    subt.textContent = 'SUB-T'
    document.querySelector('table').appendChild(subt)

    const rmv = document.createElement('th')
    rmv.textContent = 'REMOVER'
    document.querySelector('table').appendChild(rmv)
}

const incluiNaDom = (item) => {
    const docElement = document.createElement('tr')
    const tdElement6 = document.createElement('td')
    const tdElement7 = document.createElement('td')
    const tdElement8 = document.createElement('td')
    const tdElement9 = document.createElement('td')
    const tdElement10 = document.createElement('td')
    const button = document.createElement('button')    
    const subTotal = item.qtd * item.preco
        
    tdElement6.textContent = `${item.qtd}`
    docElement.appendChild(tdElement6)

    tdElement7.setAttribute('class', 'item')
    tdElement7.textContent = `${item.produto}`
    docElement.appendChild(tdElement7)

    tdElement8.textContent = `R$${item.preco.toFixed(2).replace('.', ',')}`
    docElement.appendChild(tdElement8)

    tdElement9.setAttribute('class', 'subtotal')
    tdElement9.textContent = `${subTotal.toFixed(2)}`
    docElement.appendChild(tdElement9)

    docElement.appendChild(tdElement10)
    button.textContent = 'X'
    if(item.produto !== 'Embalagem' && item.tipo !== 'taxa' && item.produto !== 'Embalagem Sup.'){
        tdElement10.appendChild(button)
    }

    button.addEventListener('click', () => {
        if(item.produto === 'Embalagem' || item.tipo === 'taxa'){
            alert('Não é possível remover')
        }else if(item.produto === 'Embalagem Sup.'){
            alert('Não é possível remover')
        }else if(item.tipo === 'bebidas' || item.tipo === 'adicionais'){
            const h = pedidos.indexOf(item)
            removePedidos1(h)
        }else{
            const a = pedidos.indexOf(item)
            //removePedidos1(a)
            removePedidos(a)
        }
        
    })

    const bairroNoCad = getCadastro()[0].nomeDoBairro
    const TAXADEENTREGA = item.produto.includes(`TAXA DE ENTREGA ${bairroNoCad}`)
    const OutrosBairros = item.produto.includes(`Outros bairros - a taxa será informada no final(${bairroNoCad})`)

    if(item.tipo === 'taxa' && (getPedidos().map((x) => x.produto === 'taxa').includes(OutrosBairros) && getPedidos().map((x) => x.produto === 'taxa').includes(TAXADEENTREGA)) || getPedidosExclui().length < 1){
        
        if(item.produto === 'Retirar na loja'){

        }else{
            if(item.tipo === 'taxa'){
                const e = pedidos.indexOf(item)
                removePedidos1(e)
                console.log(item)
            }
        }
        
    }
    
    if(item.produto === 'Retirar na loja' && getPedidosExclui().length > 0){
        const e = pedidos.indexOf(item)
        apagaPedidosExclui()
        criaPedidosExclui(1)
        removePedidos1(e)
    }
        
    return docElement
}

const insereCart = () => {
    pedidos.forEach((item) => {
        document.querySelector('table').appendChild(incluiNaDom(item))   
    }) 
}

const rodapeCart = () => {
    const trnew = document.createElement('tr')
    document.querySelector('table').appendChild(trnew)
    

    const tdElTotal = document.createElement('td')
    tdElTotal.setAttribute('colspan', '4')
    tdElTotal.setAttribute('id', 'totalTXT')
    tdElTotal.textContent = "TOTAL"
    trnew.appendChild(tdElTotal)

    const els = document.querySelectorAll('.subtotal')
    let somando = 0;
    [].forEach.call(els, function(el){
        somando += parseInt(el.textContent);
    })

    const valorTotal = document.createElement('td')
    valorTotal.setAttribute('id', 'total')
    trnew.appendChild(valorTotal)
    document.querySelector('#total').textContent = 'R$' + getPedidos().map((e) => e.subt).reduce((a, b) => {
        return a + b
    }, 0).toFixed(2).replace('.', ',')
}

pedidos = loadPedidos()

export {cartTable, insereCart, rodapeCart}