import {getPedidos} from './pedidos'
import {getClientes} from './cliente'
import { getCadastro } from './cadastro'

if(getPedidos().map((x)=> x.tipo).includes('taxa')){
   
}else{
   location.assign('./confcli.html')
}

if(getCadastro().length < 1){
    location.assign('./index.html')
}

const usuarioNome = document.querySelector('#usuario')
usuarioNome.textContent = getCadastro()[0].nome

const filtrataxa = getPedidos().filter((x) => x.tipo === 'taxa')


const total = getPedidos().map((e) => e.subt).reduce((a, b) => {
    return a + b
}, 0)

document.querySelector('.btnFim').addEventListener('click', (e) => {
    e.preventDefault()

    if(total - filtrataxa[0].preco > 100){
        location.assign(`https://wa.me/558131283238?text=%2ANome%2A%3A%20${getCadastro()[0].nome.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0A%0A%2ACEP%2A%3A%20${getCadastro()[0].cep}%0A%0A%2AEndereco%2A%3A%20${getCadastro()[0].rua.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%20${getCadastro()[0].n}%0A%0A%2ABairro%2A%3A%20${getCadastro()[0].nomeDoBairro.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0A%0A%2ACelular%2A%3A%20${getCadastro()[0].cel}%0A%0A%2APonto%20de%20referencia%2A%3A%20${getCadastro()[0].pontoDeReferencia.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getCadastro()[1]}%0A%0A%2ATroco%2A%3A%20${getCadastro()[2]}%0A%0A%2AObservacao%2A%3A%20${getCadastro()[3]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().filter((x) => x.tipo !== 'taxa').map((x) => [x.qtd, 'Cod: ' + x.nrDoPedido, x.produto.normalize('NFD').replace(/[\u0300-\u036f]/g, ''), 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AENTREGA%2A%3A%20%0A${getPedidos().filter((x) => x.tipo === 'taxa').map((x) => [x.qtd, x.produto.normalize('NFD').replace(/[\u0300-\u036f]/g, ''), 'R$ ' + 0].join(' '))}%0A%0A%2AValor%20total%2A%3A%20R$${getPedidos().filter((x) => x.tipo !== 'taxa').map((e) => e.subt).reduce((a, b) => {
            return a + b
        }, 0).toFixed(2).replace('.', ',')}`)
    }else{
        location.assign(`https://wa.me/558131283238?text=%2ANome%2A%3A%20${getCadastro()[0].nome.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0A%0A%2ACEP%2A%3A%20${getCadastro()[0].cep}%0A%0A%2AEndereco%2A%3A%20${getCadastro()[0].rua.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%2C%20${getCadastro()[0].n}%0A%0A%2ABairro%2A%3A%20${getCadastro()[0].nomeDoBairro.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0A%0A%2ACelular%2A%3A%20${getCadastro()[0].cel}%0A%0A%2APonto%20de%20referencia%2A%3A%20${getCadastro()[0].pontoDeReferencia.normalize('NFD').replace(/[\u0300-\u036f]/g, '')}%0A%0A%2AForma%20de%20pagamento%2A%3A%20${getCadastro()[1]}%0A%0A%2ATroco%2A%3A%20${getCadastro()[2]}%0A%0A%2AObservacao%2A%3A%20${getCadastro()[3]}%0A%0A%2APEDIDO%2A%3A%20%0A${getPedidos().map((x) => [x.qtd, 'Cod: ' + x.nrDoPedido, x.produto.normalize('NFD').replace(/[\u0300-\u036f]/g, ''), 'R$ ' + x.subt.toFixed(2).replace('.', ',')].join(' ')).join(' -%0A')}%0A%0A%2AValor%20total%2A%3A%20R$${getPedidos().map((e) => e.subt).reduce((a, b) => {
        return a + b
    }, 0).toFixed(2).replace('.', ',')}`)
    }

    

})