import {insereendereco} from './endereco'
import {criaPedidos, getPedidos} from './pedidos'
import {getClientes} from './cliente'

if(getPedidos().filter((x) => x.nomeDoBairro).length > 0){
    location.assign('./confirma.html')
}

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})

const emcasa = document.querySelector('.emcasa')
emcasa.addEventListener('click', (e) => {
    e.preventDefault()
    
    const formadeentrega = document.querySelector('#formaRecebimento')
    formadeentrega.setAttribute('style', 'display: none;')
    const botao2 = document.querySelector('.btn-goBack')
    botao2.setAttribute('style', 'display: none;')
    const form = document.querySelector('.inativo')
    form.classList.remove('inativo')
})

const naloja = document.querySelector('.peganaloja')
naloja.addEventListener('click', (e) => {
    e.preventDefault
    criaPedidos(1, 'Retirada na loja' , 0, 'taxa', 'Retirada na loja', 'Delivery')
    location.assign('./confirma.html')
})

    const pegaoCep = document.querySelector('#localizacep')
    pegaoCep.addEventListener('input', () => {
    const url = 'https://viacep.com.br/ws/' + pegaoCep.value.replace('-', '') +  '/json/'
    fetch(url)
        .then((r) => r.json())
        .then(data => {
            if(data.logradouro == undefined || data.bairro == undefined || data.localidade  == undefined || data.uf  == undefined){
                alert('Endereço inválido')
                const rua = document.querySelector('#endereco')
                rua.value = ''
                const bairr = document.querySelector('#bairro')
                bairr.value = ''
                const city = document.querySelector('#cidade')
                city.value = ''
            }else{
                const rua = document.querySelector('#endereco')
                rua.value = data.logradouro
                const bairr = document.querySelector('#bairro')
                bairr.value = data.bairro
                const city = document.querySelector('#cidade')
                city.value = data.localidade + ' - ' + data.uf 
            }
            
        })
        
})

const voltar1 = document.querySelector('#rtn')
voltar1.addEventListener('click', (e) => {
    e.preventDefault()
    const css = document.querySelector('.ativado')
    css.classList.add('inativo')
    const formadeentrega = document.querySelector('#formaRecebimento')
    formadeentrega.removeAttribute('style')
    const botao2 = document.querySelector('.btn-goBack')
    botao2.removeAttribute('style')
})

if(getClientes().length > 0){
    const cep = document.querySelector('#localizacep')
    const rua = document.querySelector('#endereco')
    const n = document.querySelector('#nro')
    const ref = document.querySelector('#rfr')
    const bairr = document.querySelector('#bairro')
    const city = document.querySelector('#cidade')

    cep.value = getClientes()[1]
    rua.value = getClientes()[2] 
    n.value = getClientes()[9]
    ref.value = getClientes()[4]
    bairr.value = getClientes()[3]
    city.value = getClientes()[10]
    
}

const insereEnd = document.querySelector('#endok')
insereEnd.addEventListener('click', (e) => {
    e.preventDefault()
    const cep = document.querySelector('#localizacep').value
    const rua = document.querySelector('#endereco').value
    const n = document.querySelector('#nro').value
    const ref = document.querySelector('#rfr').value
    const bairr = document.querySelector('#bairro').value
    const city = document.querySelector('#cidade').value
    if(cep === '' || rua === '' || ref === '' || n === ''){
        alert('CEP, endereço, número e ponto de referência são obrigatórios')
    }else{
        insereendereco(cep, rua, ref, bairr, city, n)
    }

    if(bairr === 'Boa Vista' || bairr === 'Soledade' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 4.9, 'taxa', bairr, 'Delivery')
        location.assign('./confirma.html')
    }else if(bairr === 'Derby' || bairr === 'Ilha do Leite' || bairr === 'Santo Antônio' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 5.9, 'taxa', bairr, 'Delivery')
        location.assign('./confirma.html')
    }else if(bairr === 'Ilha do Retiro' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 6, 'taxa', bairr, 'Delivery')
    }else if(bairr === 'Afogados' || bairr === 'Prado' || bairr === 'Bongi' || bairr === 'San Martin' || bairr === 'Torrões' || bairr === 'Casa Amarela' || bairr === 'Parnamirim' || bairr === 'Casa Forte' || bairr === 'Sítio Novo' || bairr === 'Brasília Teimosa' || bairr === 'Pina' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 8.9, 'taxa', bairr, 'Delivery')
        location.assign('./confirma.html')
    }else if(bairr === 'Madalena' || bairr === 'Encruzilhada' || bairr === 'Caxangá' || bairr === 'Arruda' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 7.9, 'taxa', bairr, 'Delivery')
        location.assign('./confirma.html')
    }else if(bairr === 'Graças' || bairr === 'Espinheiro' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 6.9, 'taxa', bairr, 'Delivery')
        location.assign('./confirma.html')
    }else if(bairr === 'Imbiribeira' || bairr === 'Ipsep'|| bairr === "Caixa D'Água" || bairr === 'Boa Viagem' || bairr === 'Várzea' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'TAXA DE ENTREGA ' + bairr , 9.9, 'taxa', bairr, 'Delivery')
        location.assign('./confirma.html')
    }else if(bairr !== 'Boa Vista' && bairr !== 'Derby' && bairr !== 'Ilha do Leite' && bairr !== 'Santo Antônio' && bairr !== 'Ilha do Retiro' && bairr !== 'Afogados' && bairr !== 'Prado' && bairr !== 'Bongi' && bairr !== 'San Martin' && bairr !== 'Torrões' && bairr !== 'Casa Amarela' && bairr !== 'Parnamirim' && bairr !== 'Casa Forte' && bairr !== 'Sítio Novo' && bairr !== 'Brasília Teimosa' && bairr !== 'Pina' && bairr !== 'Madalena' && bairr !== 'Encruzilhada' && bairr !== 'Caxangá' && bairr !== 'Arruda' && bairr !== 'Graças' && bairr !== 'Espinheiro' && bairr !== 'Imbiribeira' && bairr !== 'Ipsep' && bairr !== 'Boa Viagem' && bairr !== 'Várzea' && cep !== '' && rua !== '' && ref != '' && n !== ''){
        criaPedidos(1, 'Outros bairros - a taxa será informada no final' + '(' + bairr + ')', 0, 'taxa', 'Outros Bairros', 'Delivery')
        location.assign('./confirma.html')
    }

    
})