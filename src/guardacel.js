let guardaCel = []

const loadguardaCel = function(){
    const guardaCelJSON = sessionStorage.getItem('guardaCel')
    
    if(guardaCelJSON !== null){
        return JSON.parse(guardaCelJSON)
    } else {
        return []
    }
}

const saveguardaCel = function(){
    sessionStorage.setItem('guardaCel', JSON.stringify(guardaCel))
}

//expose orders from module
const getguardaCel = () => guardaCel

const criaguardaCel = (tel) =>{
    guardaCel.push(tel)
    saveguardaCel()
}

const removeguardaCel = (item) => {
    guardaCel.splice(item, 1)
    saveguardaCel()
    window.location.reload()
}

guardaCel = loadguardaCel()

export { getguardaCel, criaguardaCel, saveguardaCel, removeguardaCel}