let clientes = []

const loadClientes = function(){
    const clientesJSON = localStorage.getItem('clientes')
    
    if(clientesJSON !== null){
        return JSON.parse(clientesJSON)
    } else {
        return []
    }
}

const saveClientes = function(){
    localStorage.setItem('clientes', JSON.stringify(clientes))
}

//expose orders from module
const getClientes = () => clientes

//funcao para add clientes
const insereClientes = (nome, cep, end, bairro, pRef, cel, pg, troco, obs, n, cidade) => {
    clientes.unshift(nome, cep, end, bairro, pRef, cel, pg, troco, obs, n, cidade)
    saveClientes()
}

//funcao para limpar registro do cliente
const removeClientes = () => {
    localStorage.clear()
}

const mudaDados = (pgNEW, trocoNEW, obsNEW, n, c) => {
    clientes.splice(6,5)
    clientes.push(pgNEW, trocoNEW, obsNEW, n, c)
    saveClientes()
}

clientes = loadClientes()

export {getClientes, insereClientes, removeClientes, saveClientes, mudaDados}