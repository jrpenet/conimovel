const path = require('path')

module.exports = {
    entry: {
        index: ['babel-polyfill', './src/index.js'],
        cardapio: ['babel-polyfill', './src/cardapio.js'],
        conftemp: ['babel-polyfill', './src/conftemp.js'],
        confadd: ['babel-polyfill', './src/confadd.js'],
        desserts: ['babel-polyfill', './src/desserts.js'],
        beverage: ['babel-polyfill', './src/beverage.js'],
        promocoes: ['babel-polyfill', './src/promocoes.js'],
        confirma: ['babel-polyfill', './src/confirma.js'],
        txentrega: ['babel-polyfill', './src/txentrega.js'],
        txentrega2: ['babel-polyfill', './src/txentrega2.js'],
        forms: ['babel-polyfill', './src/forms.js'],
        confcli: ['babel-polyfill', './src/confcli.js'],
        enviapedidos: ['babel-polyfill', './src/enviapedidos.js']
    },
    output: {
        path: path.resolve(__dirname, 'public/scripts'),
        filename: '[name]-bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/scripts/'
    },
    devtool: 'source-map'
}